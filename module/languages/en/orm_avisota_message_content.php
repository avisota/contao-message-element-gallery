<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-element-gallery
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSources'] = array(
    'Source files',
    'Please select one or more files or folders from the files directory. If you select a folder, its files will be included automatically.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['perRow']       = array(
    'Thumbnails per row',
    'The number of image thumbnails per row.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['sortBy']       = array(
    'Order by',
    'Please choose the sort order.'
);
