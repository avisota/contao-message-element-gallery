<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:44+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSources']['0'] = 'Datotecas da funtauna';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSources']['1'] = 'Tscherna ina u pliras datotecas u ordinaturs da la structura d\'ordinaturs. Sche ti tschernas in ordinatur vegnan tut las datotecas lien integradas automaticamain';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['multiSRC']['0']     = 'Datotecas da funtauna';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['multiSRC']['1']     = 'Tscherna ina u pliras datotecas u ordinaturs da la structura d\'ordinaturs. Sche ti tschernas in ordinatur vegnan tut las datotecas lien integradas automaticamain.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['perRow']['0']       = 'Miniaturas per lingia';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['perRow']['1']       = 'Il dumber da miniaturas da maletgs per lingia.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['sortBy']['0']       = 'Zavrar tenor';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['sortBy']['1']       = 'Tscherna la successiun da zavrar.';
