<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:42+01:00
 */

$GLOBALS['TL_LANG']['MCE']['gallery']['0'] = 'Galerie';
$GLOBALS['TL_LANG']['MCE']['gallery']['1'] = 'Erzeugt eine Lightbox-Bildgalerie.';
