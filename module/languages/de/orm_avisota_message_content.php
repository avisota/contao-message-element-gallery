<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:43+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSources']['0'] = 'Quelldateien';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSources']['1'] = 'Wählen Sie eine oder mehrere Dateien aus der Dateiverwaltung aus. Falls Sie einen Ordner auswählen werden die darin enthaltenen Dateien automatisch hinzugefügt.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['perRow']['0']       = 'Vorschaubilder pro Reihe';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['perRow']['1']       = 'Bitte geben Sie die Anzahl der Vorschaubilder pro Reihe an.,';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['sortBy']['0']       = 'Sortieren nach ...';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['sortBy']['1']       = 'Bitte wählen Sie die Sortierreihenfolge aus.';
